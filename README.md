[![pipeline status](https://gitlab.com/bizops/bizops-elt/badges/master/pipeline.svg)](https://gitlab.com/bizops/bizops-elt/commits/master)

# BizOps Extract Container

This project builds the `extract` image, which contains the required dependencies to act as a base image for the ELT scripts/jobs in the [BizOps](https://gitlab.com/bizops/bizops/) project.

It contains:
* Pentaho Data Integration 7.1 with OpenJDK 8 to extract data from SFDC
* Python 3.5.3 and extraction scripts for Zuora and Marketo

#### Using the `extract` image

The container is primarily built to be used in conjunction with GitLab CI, to automate and schedule the extraction of data. Creating the container is currently a manual job, since it changes infrequently and consumes network/compute resources. To build the container initially or after changes, simply run the `extract_container` job in the `build` stage. The subsequent `extract` stage can be cancelled and restarted once the container has finished building. This will be improved in the future.

Together with the `.gitlab-ci.yml` file and [project variables](https://docs.gitlab.com/ce/ci/variables/README.html#protected-secret-variables), it is easy to configure. Simply set the following variables in your project ensure that the container is available.
* PG_ADDRESS: IP/DNS of the Postgres server.
* PG_PORT: Port number of the Postgres server, typically 5432.
* PG_DATABASE: Database name to be used for the staging tables.
* PG_DEV_SCHEMA: Schema to use for development of dbt models.
* PG_PROD_SCHEMA: Schema to use for production dimensional model.
* PG_USERNAME: Username for authentication to Postgres.
* PG_PASSWORD: Password for authentication to Postgres.
* GCP_PRODUCTION_INSTANCE_NAME: Cloud Production SQL Instance Name. Set if wanting to use Cloud SQL Proxy.
* GCP_SERVICE_CREDS: GCP Service Credentials JSON. Set if wanting to use Cloud SQL Proxy.
* SFDC_URL: Web service URL for your SFDC account.
* SFDC_USERNAME: Username for authentication to SFDC.
* SFDC_PASSWORD: Password for authentication to SFDC.
* ZUORA_URL: Web service URL for your Zuora account.
* ZUORA_USERNAME: Username for authentication to Zuora.
* ZUORA_PASSWORD: Password for authentication to Zuora.
